package sample.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import sample.entities.Employee;

/**
 * Created by amarchuk on 06.05.2014.
 */
public interface EmployeeRepository extends CrudRepository<Employee, Long> {

    @Query("SELECT e FROM Employee e WHERE e.employeeId = ?1")
    Employee find(Long employeeId);
}
