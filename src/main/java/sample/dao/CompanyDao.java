package sample.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sample.entities.Employee;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by Alexander Marchuk on 04.05.2014.
 */
@Repository
@Transactional
public class CompanyDao {

    @PersistenceContext(unitName = "entityManager")
    private EntityManager entityManager;

    public void add(Object o) {
        entityManager.persist(o);
        entityManager.flush();
    }

    public void update(Object o) {
        entityManager.merge(o);
    }

    public <T> T loadById(Class<T> clazz, long id) {
        return entityManager.find(clazz, id);
    }

    public List<Employee> loadAllEmployees() {
        return entityManager.createQuery("SELECT e FROM Employee e").getResultList();
    }
}
