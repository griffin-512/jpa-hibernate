package sample.entities;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Alexander Marchuk on 04.05.2014.
 */

@Entity
@Table(name = "customer")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String name;

    @OneToMany(mappedBy = "customer", cascade = CascadeType.REMOVE)
    private List<Employee> employees;

    public long getId() {
        return id;
    }

    public void setId(long customerId) {
        this.id = customerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "customerId=" + id +
                ", name='" + name + '\'' +
                ", employees=" + employees +
                '}';
    }
}
