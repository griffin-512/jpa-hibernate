package sample.dao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import sample.entities.Company;
import sample.entities.Customer;
import sample.entities.Employee;
import sample.repositories.EmployeeRepository;

import java.sql.Date;

/**
 * Created by Alexander Marchuk on 04.05.2014.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext.xml" })
@Transactional
public class CompanyDaoTest {

    @Autowired
    private CompanyDao companyDao;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Before
    public void testSave() {
        Company company = new Company();
        company.setAddress("Springfield, Evergreen Terrace");
        company.setName("Microsoft");

        companyDao.add(company);

        Employee employee = new Employee();
        employee.setCompany(companyDao.loadById(Company.class, 1));
        employee.setFirstName("Homer");
        employee.setLastName("Simpson");
        employee.setHireDate(new Date(new java.util.Date().getTime()));
        employee.setSalary(5000);

        companyDao.add(employee);

        Customer customer = new Customer();
        customer.setName("HP");

        companyDao.add(customer);

        employee = new Employee();
        employee.setCompany(companyDao.loadById(Company.class, 1));
        employee.setCustomer(companyDao.loadById(Customer.class, 1));
        employee.setFirstName("Bart");
        employee.setLastName("Simpson");
        employee.setHireDate(new Date(new java.util.Date().getTime()));
        employee.setSalary(5000);

        companyDao.add(employee);
    }

    @Test
    public void testLoad() {
        System.out.println(companyDao.loadById(Employee.class, 1));
        System.out.println(companyDao.loadById(Company.class, 1));
        System.out.println(companyDao.loadAllEmployees());
        System.out.println(employeeRepository.find(2L));
    }
}
